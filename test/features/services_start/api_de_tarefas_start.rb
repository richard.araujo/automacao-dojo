class ApiDeTarefasStart < SitePrism::Page
    def listar_todos_contatos
        @response = ApiDetarefas.get("/contacts")
    end   

    def criar_contato
        @body = { 
            "name": $name_faker, 
            "email": $mail_faker,
            "age": $age_faker,
            "phone": $phone_faker,
            }.to_json
        @response = ApiDetarefas.post("/contacts", body: @body)
        $RETURNS['contacts']['id'] << @response["data"]["id"]
    end

    def listar_unico_contato
        @response =  ApiDetarefas.get("/contacts/#{$RETURNS['contacts']['id']}")
    end

    def deletar_contato
        @response =  ApiDetarefas.delete("/contacts/#{$RETURNS['contacts']['id']}")
    end

    def editar_contato
        @body = { 
            "name": "ALTERADO PARA O PINTUPLAYS",
            "email": $mail_faker,
            "age": "28",
            "phone": "11963551747",
          }.to_json
        @response = ApiDetarefas.put("/contacts/#{$RETURNS['contacts']['id']}", body: @body)
    end
end