Dir[File.join(File.dirname(__FILE__), '../services_start/*_start.rb')].each { |file| require file }

# Modulos para chamar as classes instanciadas
module Page
    def api_tarefas
      api_tarefas ||= ApiDeTarefasStart.new
    end 
end
