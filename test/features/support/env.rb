require 'capybara'
require 'capybara/cucumber'
require 'capybara/dsl'
require 'capybara/rspec/matchers'
require 'faker'
require 'rspec'
require 'site_prism'
require 'report_builder'
require 'json'
require 'httparty'
require 'httparty/request'
require 'httparty/response/headers'


require_relative 'service_helper.rb'

World Page

World Capybara::DSL
World Capybara::RSpecMatchers

AMB = ENV['AMB']

BASE_URL = YAML.load_file(File.dirname(__FILE__) + "/ambientes/ambientes.yml")[AMB]
$RETURNS = YAML.load_file(File.dirname(__FILE__) + "/loads/returns.yml")

