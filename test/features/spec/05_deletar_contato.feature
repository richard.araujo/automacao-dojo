#language: pt
Funcionalidade: Efetuar a exclusao de um contato
    @deletar_contato
    Cenario: Deletar contato
        Dado que eu faco um DELETE da API
        Entao recebo o resultado que foi deletado
    