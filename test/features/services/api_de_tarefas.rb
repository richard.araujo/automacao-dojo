module ApiDetarefas
    include HTTParty
    #Url padrão 
    base_uri BASE_URL['url_base']
    # Opções do service
    format :json
    # Por algum motivo que nao tiver tempo de Investigar a API recebe Header no postman mas quando usada com HTTParty nao necessita
     headers 'Content-Type': 'application/json',
        'Accept': 'application/vnd.tasksmanager.v2',
        'Location': '/contacts/1'

  end