Dado('que eu faco um GET da API de contatos') do
  @response = api_tarefas.listar_unico_contato
end

Entao('recebo um resultado unico') do
  log @response["data"]
  #raise "Erro: o codido retornado esta errado!" if @response.code != 200
  #raise "Erro: A api esta retornando vazio" if @response["data"] == nil
end
