Dado('que eu faco um PUT da API') do
  @response = api_tarefas.editar_contato
end

Entao('recebo o resultado da alteracao') do
  log @response.to_json
  raise "Erro: o codido retornado esta errado!" if @response.code != 200
  raise "Erro: A api esta retornando vazio" if @response["data"] == nil
end