Dado('que eu faco um DELETE da API') do
  @response = api_tarefas.deletar_contato
end

Entao('recebo o resultado que foi deletado') do
  @response_valida =  ApiDetarefas.get("/contacts/#{$RETURNS['contacts']['id']}")
  log @response
  raise "Erro: o codido retornado esta errado!" if @response_valida.code != 404
end