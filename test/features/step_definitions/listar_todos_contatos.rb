
Dado('que eu faco um GET da API') do
    @response = api_tarefas.listar_todos_contatos
    
end

Entao('recebo um resultado') do
    raise "Erro: o codido retornado esta errado!" if @response.code != 200
    raise "Erro: A api esta retornando vazio" if @response["data"] == nil
    log @response.to_json
end

